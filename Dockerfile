# ステップ1: ビルド環境を構築
FROM golang:1.21.4-bookworm AS builder

# 作業ディレクトリの設定
WORKDIR /app

# モジュールファイルをコピー
COPY go.mod ./
# COPY go.sum ./

# モジュールのダウンロード
RUN go mod download

# ソースコードをコピー
COPY *.go ./

# Goアプリケーションをビルド
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# ステップ2: 実行環境を構築
FROM alpine:3.18.5

# SSL証明書をインストール
RUN apk --no-cache add ca-certificates

# 作業ディレクトリの設定
WORKDIR /root/

# ビルドしたバイナリをコピー
COPY --from=builder /app/main .

# APIサーバーを起動
CMD ["./main"]